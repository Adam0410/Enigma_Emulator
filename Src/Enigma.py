from random import randint
from time import sleep

mode = "normal"
plugWireCount = 10
plugPair1 = []
plugPair2 = []
alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
rotor1 = ["j", "p", "g", "v", "o", "u", "m", "f", "y", "q", "b", "e", "n", "h", "z", "r", "d", "k", "a", "s", "x", "l", "i", "c", "t", "w"]
rotor2 = ["n", "z", "j", "h", "g", "r", "c", "x", "m", "y", "s", "w", "b", "o", "u", "f", "a", "i", "v", "l", "p", "e", "k", "q", "d", "t"]
rotor3 = ["f", "k", "q", "h", "t", "l", "x", "o", "c", "b", "j", "s", "p", "d", "z", "r", "a", "m", "e", "w", "n", "i", "u", "y", "g", "v"]
reflector = ["e", "n", "k", "q", "a", "u", "y", "w", "j", "i", "c", "o", "p", "b", "l", "m", "d", "x", "z", "v", "f", "t", "h", "r", "g", "s"]


def readCommand(command):
    global mode

    if mode == "plugboard":

        if command == "list pairs":
            print ""
            changePlugs("", "list")
            print ""
            return True
        elif command[:7] == "connect":
            print ""
            result = changePlugs(command[8:], "connect")

            if result == True:
                print "Operation successful."
            elif result == False:
                print "Invalid command format. The format for this command is 'connect <a>-<b>', where a and b are the letters you wish to connect."
            elif result == "used letter":
                print "One or more of those letters have already been assigned to a plugboard pair."
            elif result == "no wires":
                print "You have no wires left to connect letter pairs, try disconnecting some pairs first."
            elif result == "same letter":
                print "You can't wire a letter to itself!"

            print ""
            return True
        elif command[:10] == "disconnect":
            print ""
            result = changePlugs(command[11:], "disconnect")

            if result == True:
                print "Operation successful."
            elif result == False:
                print "Invalid command format. The format for this command is 'disconnect <a>-<b>', where a and b are the letters you wish to disconnect."
            elif result == "invalid pair":
                print "That pair of letters does not exist, try typing 'list pairs' to get a list of currently connected letters."
            elif result == "same letter":
                print "You can't disconnect a letter from itself!"

            print ""
            return True
        elif command == "remaining wires":
            print ""
            print "You currently have "+str(plugWireCount)+" wires left to connect letters with."
            print ""
            return True
        elif command == "help":
            printPlugboardHelp()
            return True
        elif command == "exit":
            mode = "normal"
            print ""
            print "Returned to normal operation."
            print ""
            return True
        elif command == "clear plugboard":
            print ""
            print "Are you sure y/n?"
            print ""

            while True:
                confirm = raw_input("> ")
                confirm = confirm.lower()

                if confirm == "y" or confirm == "yes":
                    print ""
                    print "All plugboard settings cleared."
                    print ""
                    changePlugs("", "clear")
                    break
                elif confirm == "n" or confirm == "no":
                    print ""
                    print "Plugboard clear aborted."
                    print ""
                    break
                else:
                    print ""
                    print "Invalid command, type y/n."
                    print ""

            return True
        else:
            print ""
            print "Invalid command, try typing 'help' for a list of commands."
            print ""
            return True

    elif mode == "normal":

        if command == "plugboard":
            mode = "plugboard"
            print ""
            print "Entered plugboard mode. Try typing 'help' for help with this mode and a list of commands."
            print ""
            return True
        elif command == "help":
            printHelp()
            return True
        elif command == "exit":
            print "Saving settings and exiting..."
            sleep(1.5)
            return "exit"
        elif command[:9] == "set rotor":
            result = setRotorPos(command[10:])

            if result == True:
                print ""
                print "Operation successful."
                print ""
                return True
            else:
                print ""
                print "Invalid command format. The format for this command is 'set rotor <rotor num> <letter>' where rotor num is the number of rotor you wish to change up to rotor number 3, and letter is the letter you wish to set it to."
                print ""
                return True

        elif command == "clear rotors":
            print ""
            print "Are you sure y/n?"
            print ""

            while True:
                confirm = raw_input("> ")
                confirm = confirm.lower()

                if confirm == "y" or confirm == "yes":
                    print ""
                    print "All rotors reset."
                    print ""
                    resetRotors()
                    break
                elif confirm == "n" or confirm == "no":
                    print ""
                    print "Rotor reset aborted."
                    print ""
                    break
                else:
                    print ""
                    print "Invalid command, type y/n."
                    print ""

            return True
        elif command == "clear all settings":
            print ""
            print "Are you sure y/n?"
            print ""

            while True:
                confirm = raw_input("> ")
                confirm = confirm.lower()

                if confirm == "y" or confirm == "yes":
                    print ""
                    print "All settings cleared."
                    print ""
                    resetRotors()
                    changePlugs("", "clear")
                    break
                elif confirm == "n" or confirm == "no":
                    print ""
                    print "Settings clear aborted."
                    print ""
                    break
                else:
                    print ""
                    print "Invalid command, type y/n."
                    print ""

            return True
        elif command[:7] == "convert":
            result, conversion = convertText(command[8:])

            if result == False:
                print ""
                print "The text you've entered contains characters other than those contained within the English alphabet, which this program does not support. Please not spaces are also not supported."
                print ""
            else:
                print ""
                print "Converted text is: "+conversion
                print ""
        else:
            print ""
            print "Invalid command. Try typing 'help' for a list of commands."
            print ""
            return True










def changePlugs(command, operation):
    global plugWireCount, plugPair1, plugPair2

    if operation == "connect":

        if len(command) == 3 and command[0].isalpha() and command[1] == "-" and command[2].isalpha():

            if command[0] == command[2]:
                return "same letter"

            for char in plugPair1:

                if command[0] == char or command[2] == char:
                    return "used letter"

            else:
                for char in plugPair2:

                    if command[0] == char or command[2] == char:
                        return "used letter"

                else:

                    if plugWireCount > 0:
                        plugPair1.append(command[0])
                        plugPair2.append(command[2])
                        plugWireCount -= 1
                        return True
                    else:
                        return "no wires"
        else:
            return False

    elif operation == "disconnect":

        if len(command) == 3 and command[0].isalpha() and command[2].isalpha():

            if command[0] == command[2]:
                return "same letter"

            i = 0

            for char in plugPair1:

                if command[0] == char and command[2] == plugPair2[i]:
                    plugPair1.remove(command[0])
                    plugPair2.remove(command[2])
                    plugWireCount += 1
                    return True
                elif command[2] == char and command[0] == plugPair2[i]:
                    plugPair1.remove(command[2])
                    plugPair2.remove(command[0])
                    plugWireCount += 1
                    return True

                i += 1

            else:
                return "invalid pair"

        else:
            return False

    elif operation == "list":

        if len(plugPair1) == 0:
            print "There are no currently connected letter pairs."
            return True
        else:
            i = 0

            for char in plugPair1:
                print char+"-"+plugPair2[i]
                i += 1

            return True
    elif operation == "clear":
        plugWireCount = 10
        plugPair1 = []
        plugPair2 = []
        return True
    else:
        return "invalid operation"


def plugConversion(char):
    plugged = False
    i = 0

    for letter in plugPair1:

        if char == letter:
            char = plugPair2[i]
            plugged = True
            break

        i += 1

    i = 0

    if not plugged:

        for letter in plugPair2:

            if char == letter:
                char = plugPair1[i]
                break

            i += 1

    return char


def shuntRotors():
    global rotor1, rotor2, rotor3

    char = rotor1[len(rotor1)-1]
    rotor1.remove(char)
    rotor1.insert(0, char)

    if rotor1[0] == "a" or rotor1[0] == "n":
        char = rotor2[len(rotor2)-1]
        rotor2.remove(char)
        rotor2.insert(0, char)

    if rotor2[0] == "a" or rotor2[0] == "n":
        char = rotor3[len(rotor3)-1]
        rotor3.remove(char)
        rotor3.insert(0, char)


def resetRotors():
    global rotor1, rotor2, rotor3

    rotor1 = ["j", "p", "g", "v", "o", "u", "m", "f", "y", "q", "b", "e", "n", "h", "z", "r", "d", "k", "a", "s", "x", "l", "i", "c", "t", "w"]
    rotor2 = ["n", "z", "j", "h", "g", "r", "c", "x", "m", "y", "s", "w", "b", "o", "u", "f", "a", "i", "v", "l", "p", "e", "k", "q", "d", "t"]
    rotor3 = ["f", "k", "q", "h", "t", "l", "x", "o", "c", "b", "j", "s", "p", "d", "z", "r", "a", "m", "e", "w", "n", "i", "u", "y", "g", "v"]


def rotorConversion(char, rotor, reverse=False):

    if reverse:

        if rotor == 1:
            num = rotor1.index(char)
            result = alphabet[num]
        elif rotor == 2:
            num = rotor2.index(char)
            result = alphabet[num]
        elif rotor == 3:
            num = rotor3.index(char)
            result = alphabet[num]
        else:
            num = alphabet.index(char)
            result = reflector[num]

    else:
        num = alphabet.index(char)

        if rotor == 1:
            result = rotor1[num]
        elif rotor == 2:
            result = rotor2[num]
        elif rotor == 3:
            result = rotor3[num]
        else:
            result = reflector[num]

    return result


def convertText(text):
    result = ""

    for char in text:

        if char.isalpha():
            char = plugConversion(char)
            shuntRotors()
            char = rotorConversion(char, 1)
            char = rotorConversion(char, 2)
            char = rotorConversion(char, 3)
            char = rotorConversion(char, "reflector")
            char = rotorConversion(char, 3, True)
            char = rotorConversion(char, 2, True)
            char = rotorConversion(char, 1, True)
            char = plugConversion(char)
            result = result+char

        else:
            return False, ""

    return True, result



def printHelp():
    print ""
    print "--------------------Help--------------------"
    print ""
    print "This Enigma program can be used to encrypt anything you type using the same system as the germans used during World War 2."
    print "To learn more about this program, how it was made, and the original Engima system, as well as modern cryptographical techniques, please refer to my accompanying report."
    print "Any part of a command including angle brackets, <>, should be replaced with your own choice, and the angle brackets should NOT be included."
    print ""
    print "------------------Commands------------------"
    print ""
    print "'help'                           - Displays this help message."
    print "'exit'                           - Exits this program and saves the current Enigma settings."
    print "'convert <text>'                 - Converts whatever is in the place of the word text to enigma code if it is in English and vice versa, using the current Enigma settings. Only letters and spaces can be used, ignoring casing."
    print "'plugboard'                      - Enters the plugboard mode so you can change the plugboard settings."
    print "'set rotor <rotor num> <letter>' - Sets the rotor number (maximum 3) to the particular letter you choose."
    print "'clear rotors'                   - Resets all the rotors back to the default of letter A."
    print "'clear all settings'             - Resets all Engima settings, including the rotors and all the plugboard connections."
    print ""


def printPlugboardHelp():
    print ""
    print "---------------Plugboard Help---------------"
    print ""
    print "The plugboard is where you can connect letters in pairs. This acts as an additional layer of encryption, converting the letter to its pair twice during the encryption process."
    print "You have 10 wires to which you can connect the letters in pairs, meaning you can connect a total of 20 letters. If you do not connect a letter it will still be encrypted but with one less layer of security."
    print "Each letter can only be part of one pair, so it can't be connected to more than one other letter."
    print "Any part of a command including angle brackets, <>, should be replaced with your own choice, and the angle brackets should NOT be included."
    print ""
    print "------------------Commands------------------"
    print ""
    print "'help'               - Displays this help message."
    print "'exit'               - Exits the plugboard mode of the program and returns to normal operation."
    print "'list pairs'         - Lists all the currently connected letter pairs."
    print "'remaining wires'    - Tells you how many wires you have left with which to connect letter pairs."
    print "'connect <a>-<b>'    - Connects two letters in a pair, where a and b are the letters you want to connect."
    print "'disconnect <a>-<b>' - Disconnects a pair of two letters, where a and b are the two letters in a pair you want to disconnect."
    print "'clear plugboard'    - Clears all the currently connected plugboard connections."
    print ""


while True:
    command = raw_input("> ")
    command = command.lower()
    result = readCommand(command)

    if result == "exit":
        #saveSettings()
        break

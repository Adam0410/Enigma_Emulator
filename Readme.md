This program is currently written in Python 2.7.10 and is designed to emulate the Enigma machine used to encerypt and decrypt German radio 
traffic during world war 2. 

Currently the program is in a working state and a compiled executable can be downloaded from the bin folder that will work without needing a 
python interpreter on your device. It is currenty missing features however that will be added over the next few weeks along with its translation 
into C++. The features that will be added can be seen under the list of planned features below.

The program is command line only and a list of possible command can be found by typing 'help'. I decided against a GUI since my design skills are
sub-par and the GUI would take too much of my time and reduce the overall quality of the program and increase the number of bugs. 

Since this is part of my EPQ there is a report that accompanies this program that goes into details about the mechanical workings of the Enigma 
machine and also makes comparisons between the Enigma encryption mechanism and modern symmetric and asymmetric key cryptography that is used 
today to secure most of the web. 

If you'd like a brief summary of the mechanics of the Enigma machine a version for non-technical audiences can be found using the link below:
http://enigma.louisedade.co.uk/howitworks.html

## List of planned features

* Help Comamnds (If you want to use the program you'll need to read and understand the source code at the moment)
* Ability to switch between the various different rotors used in Enigma machines
* Ability to switch between the 3 and 4 rotor Enigma mechanisms
* Ability to generate daily keys that will be useful for 2 way conversations